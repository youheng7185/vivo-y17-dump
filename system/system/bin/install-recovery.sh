#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:b3a746784f53a0d7e1a3997f86017325017b3628; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:d44dc685c6f96b806a2bb5bb0655325b0aae783e EMMC:/dev/block/platform/bootdevice/by-name/recovery b3a746784f53a0d7e1a3997f86017325017b3628 67108864 d44dc685c6f96b806a2bb5bb0655325b0aae783e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
