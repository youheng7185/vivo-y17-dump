#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_PD1901F_EX.mk

COMMON_LUNCH_CHOICES := \
    omni_PD1901F_EX-user \
    omni_PD1901F_EX-userdebug \
    omni_PD1901F_EX-eng
